package scripts.BoneyardBuryer;

import java.awt.Point;

import org.tribot.api2007.types.RSTile;

//in standard form
public class Equation {
	
	private int a;
	private int b;
	private int c;
	private RSTile e_point1;
	private RSTile e_point2;
	//http://www.mathwords.com/d/distance_point_to_line.htm
	//https://stackoverflow.com/questions/13242738/how-can-i-find-the-general-form-equation-of-a-line-from-two-points
	//heavily modified for RSTile
	public Equation(RSTile point1, RSTile point2) {
		makeEquation(point1, point2);
		e_point1 = point1;
		e_point2 = point2;
	}
	
	
	private void makeEquation(RSTile point1, RSTile point2) {
		a = point1.getY() - point2.getY();
		b = point2.getX() - point1.getX();
		c = ((point1.getX() - point2.getX()) * point1.getY()) + ((point2.getY() - point1.getY()) * point1.getX());
	}
	
	public RSTile getPoint1() {
		return e_point1;
	}
	
	public RSTile getPoint2() {
		return e_point2;
	}
	
	public double getSlope() {
		return (e_point2.getY() - e_point1.getY()) / (double)(e_point1.getX() - e_point2.getX());
//		return (y2-y1) / (double) (x2-x1); 
	}
	
	public double getPerpendicularSlope() {
		return -1 * ((e_point1.getX() - e_point2.getX()) / (double)(e_point2.getY() - e_point1.getY()));
	}
	
	
	public int getA() {
		return a;
	}
	
	public int getB() {
		return b;
	}
	
	public int getC() {
		return c;
	}
	
	public double distance(RSTile point) {
		int top = Math.abs((a * point.getX()) + (b * point.getY()) + c);
		double bottom = Math.sqrt((a * a) + (b * b));
		return top / bottom;
	}

}
