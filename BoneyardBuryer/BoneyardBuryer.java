package scripts.BoneyardBuryer;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.util.Arrays;
import javax.swing.JOptionPane;
import org.tribot.api.General;
import org.tribot.api.input.Mouse;
import org.tribot.api.types.generic.Condition;
import org.tribot.api.types.generic.Filter;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Game;
import org.tribot.api2007.GameTab;
import org.tribot.api2007.GameTab.TABS;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.api2007.GroundItems;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Objects;
import org.tribot.api2007.Options;
import org.tribot.api2007.Player;
import org.tribot.api2007.Skills;
import org.tribot.api2007.Walking;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSGroundItem;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSPlayer;
import org.tribot.api2007.types.RSTile;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Painting;

@ScriptManifest (authors = {"musique"}, category = "Prayer", name = "Boneyard Buryer")

public class BoneyardBuryer extends Script implements Painting{
    
	//361 is tuna
	//333 for trout
	private int food = -1;
	public static int bigBone = 532;
	public static int littleBone = 526;
	private CameraUtils camera = new CameraUtils();
	private CommonUtils commons = new CommonUtils(camera);
	private KMUtils utils = new KMUtils(camera, commons);
	private RSPlayer player = Player.getRSPlayer();
	public final static Polygon LUMBRIDGE_HOME = createArea(new RSTile(3218, 3226), new RSTile(3235, 3210));
	private int numLittleBones = 0; //4.5 experience per bone
	private int numBigBones = 0;  //15 experience per bone
	private StopWatch stopWatch = null;
	
	int inventoryCount;
	@Override
	public void run() {
		if(!Game.isRunOn()) {
			while(!Options.setRunOn(false)){};
		}
		setFoodType();
		stopWatch = new StopWatch(); //start time
		boolean isInLumbridgeAtStart = false;
		while(true) {
			isInLumbridgeAtStart = false;
			if(isInArea(LUMBRIDGE_HOME)) {
				isInLumbridgeAtStart = true;
			}
			if(Player.getPosition().getY() < 3523) {
				equipAll();
			}
			//if there's no food in inventory, go get food.
			WebWalking.setUseRun(false);
			if(Inventory.find(food).length != 27) {
				if(!getFood()) { //if no more food, end script
					return;
				}
				if(!isInLumbridgeAtStart && isInArea(LUMBRIDGE_HOME)) {
					continue;
				}
				moveFood();
			}
			walkToBoneYard();
			if(!isInLumbridgeAtStart && isInArea(LUMBRIDGE_HOME)) {
				continue;
			}
			while(true) { 
				//if General
				if((Skills.getCurrentLevel(SKILLS.HITPOINTS) / (double)Skills.getActualLevel(SKILLS.HITPOINTS)) < General.randomDouble(0.4, 0.5)) {
					RSItem[] invent = Inventory.find(food);
					int prevLength = invent.length;
					while(Inventory.find(food).length == prevLength) {
						invent[0].click();
					}
					if(Inventory.find(food).length == 0) {
						break;
					}
				}
				RSGroundItem[] bones = GroundItems.findNearest(new Filter<RSGroundItem>() {
					@Override
					public boolean accept(RSGroundItem arg0) {
						// TODO Auto-generated method stub
						if(arg0.getID() == bigBone) {
							return true;
						} //little bone
						if(arg0.getID() == littleBone && hasNoSkeletonsNearby(arg0)) {
							return true;
						} else {
							return false;
						}
					}
				});
				Arrays.sort(bones, new BoneComparator());
				if(bones.length > 0) {
					System.out.println("bones length: " + bones.length);
					RSGroundItem bone = bones[0];
					if(skeletonsInteractingWithMe()) {
						if(!Game.isRunOn()) {
							while(!Options.setRunOn(true)){};
						}
					} else {
						while(!Options.setRunOn(false)){};
					}
					RSTile[] path = Walking.generateStraightPath(bone.getPosition());
					//if not on screen, walk, otherwise just click it on the screen
					if(!bone.isOnScreen()) {
						while(!isInArea(LUMBRIDGE_HOME) && !Walking.walkPath(path, new Condition() {
							@Override
							public boolean active() {
								// TODO Auto-generated method stub
								if((Skills.getCurrentLevel(SKILLS.HITPOINTS) / (double)Skills.getActualLevel(SKILLS.HITPOINTS)) < General.randomDouble(0.4, 0.5)) {
									RSItem[] invent = Inventory.find(food);
									int prevLength = invent.length;
									while(Inventory.find(food).length == prevLength) {
										invent[0].click();
									}
								}
								return false;
							}
						}, 10)){};
					}
					while(!utils.pickUpLoot(bone)){};
					if(bone.getID() == littleBone) {
						numLittleBones += 1;
					} else {
						numBigBones += 1;
					}
					while(!isInArea(LUMBRIDGE_HOME) && Inventory.find(bone.getID()).length > 0){
						Inventory.find(bone.getID())[0].click();
					};
				}
				if(isInArea(LUMBRIDGE_HOME)) { 
					//died
					break;
				}
				General.sleep(General.random(100, 200));
			}
		}
	}
	
	//moves first food to last place for better bone placement
	public void moveFood() {
		if(GameTab.getOpen() != TABS.INVENTORY) {
			GameTab.open(TABS.INVENTORY);
		}
		Timing.CSleep(new scripts.BoneyardBuryer.Timing.Condition() {
			@Override
			public boolean validate() {
				// TODO Auto-generated method stub
				return Inventory.getAll().length != 0;
			}}
		, 3600000);
		General.sleep(General.random(1000, 2000));
		Mouse.drag(new Point(General.random(571,  579), General.random(224, 235)), new Point(General.random(705,  708), General.random(444, 452)), 1);
	}
	
	public boolean skeletonsInteractingWithMe() {
		RSNPC[] skeletons = NPCs.findNearest("Skeleton");
		if(skeletons.length > 0) {
			for(RSNPC skeleton : skeletons) {
				if(skeleton.isInteractingWithMe()) {
					return true;
				}
			}
			return false;
		} else {
			return false;
		}
	}
	
	public void equipAll() {
		for(RSItem i : Inventory.getAll()) {
			if(i.getID() != food) {
				i.click("w");
			}
			sleep(100,300);
		}
	}
	
	public void walkToBoneYard() {
		if(isInBoneYard() && !isInArea(LUMBRIDGE_HOME)) {
			return;
		}
		if(Game.isRunOn()) {
			while(!Options.setRunOn(false)){};
		}
		WebWalking.setUseRun(false);
		if(Player.getPosition().getY() < 3523) {
			while(!WebWalking.walkTo(new RSTile(3244, 3520, 0))){};
			if(!isInArea(LUMBRIDGE_HOME)) { //if inside lumbridge, don't go into here because you dead.
				//died
				RSObject[] wildy = Objects.findNearest(5, 23271);
				if(wildy.length > 0)  {
					while(true) {
						wildy[0].click("Cross Wilderness Ditch");
						General.sleep(3000);
						if(Player.getPosition().getY() >= 3523) {
							break; //have successfully crossed wilderness
						}
					}
				}
			}
		}
		ObstacleChecker obstacleCheckerThread = new ObstacleChecker();
		obstacleCheckerThread.setDaemon(true);
		obstacleCheckerThread.start();
		while(!isInArea(LUMBRIDGE_HOME) && !WebWalking.walkTo(new RSTile(3246, 3729, 0))){};
		//shutdown obstacle checker
		obstacleCheckerThread.setIsRunning(false);
	}
	
	public boolean getFood() {
		if(isInBoneYard()) {
			while(!isInArea(LUMBRIDGE_HOME) && !WebWalking.walkTo(new RSTile(3246, 3729, 0))){};
		}
		if(isInArea(LUMBRIDGE_HOME)) {
			return true;
		}
		//inside wilderness 
		if(player.getPosition().getY() >= 3523) {
			ObstacleChecker obstacleCheckerThread = new ObstacleChecker();
			obstacleCheckerThread.setDaemon(true);
			obstacleCheckerThread.start();
			while(!isInArea(LUMBRIDGE_HOME) && !WebWalking.walkTo(new RSTile(3244, 3523, 0))){};
			if(isInArea(LUMBRIDGE_HOME)) {
				return true;
			}
			RSObject[] wildy = Objects.findNearest(5, 23271);
			if(wildy.length > 0) {
				while(true) {
					wildy[0].click("Cross Wilderness Ditch");
					General.sleep(3000);
					if(Player.getPosition().getY() < 3523) {
						break; //have successfully crossed wilderness
					}
				}
			}
			obstacleCheckerThread.setIsRunning(false);
		}
			
			if(player.getPosition().distanceTo(Banks.LUMBRIDGE.getLocation()) < player.getPosition().distanceTo(Banks.VARROCK_EAST.getLocation())) {
				while(!WebWalking.walkTo(Banks.LUMBRIDGE.getLocation())){};
				while(!MBanking.openBooth(Banks.LUMBRIDGE)) {
					sleep(General.random(10,20));	//random sleeping
				}
			} else { //go to varrock east bank
				while(!WebWalking.walkTo(Banks.VARROCK_EAST.getLocation())){};
				while(!MBanking.openBooth(Banks.VARROCK_EAST)) {
					sleep(General.random(500,700));	//random sleeping
				}
			}
			while(!Banking.isBankScreenOpen()) {
				System.out.println("bank isn'ts open");
				sleep(General.random(10,20));	//random sleeping
			}
			if(Banking.isBankScreenOpen()) {
				System.out.println("BANK IS OPEN");
			}
			Timing.CSleep(new scripts.BoneyardBuryer.Timing.Condition(){

				@Override
				public boolean validate() {
					// TODO Auto-generated method stub
					return Banking.getAll().length > 0;
				}}, 3600000);
			Banking.depositAll();
			RSItem[] item = Banking.getAll();
			System.out.println("item length: " + item.length);
			System.out.println(MBanking.withdrawItem(27, food));
			if(item.length == 0) {
				//no more items so end script
				return false;
			} else {
				return true;
			}
	}
	
	public boolean hasNoSkeletonsAlongPath(RSGroundItem bone) {
		RSNPC[] skeletons = NPCs.findNearest("Skeleton");
		if(skeletons.length > 0) {
			for(RSNPC skeleton : skeletons) {
				Equation equation = new Equation(player.getPosition(), bone.getPosition());
				if(equation.distance(skeleton.getPosition()) == 3) {
					return false;
				}
			}
			return true;
		} else {
			return true;
		}
	}
	
	public boolean hasNoSkeletonsNearby(RSGroundItem bone) {
		RSNPC[] skeletons = NPCs.findNearest("Skeleton");
		if(skeletons.length > 0) {
			for(RSNPC skeleton : skeletons) {
				if(skeleton.getPosition().distanceTo(bone.getPosition()) <= 3) {
					return false;
				}
			}
			return true;
		} else {
			return true; //no skeletons around
		}
	}
	
	boolean paintOnce = true;
	
	public boolean isInBoneYard() {
		return player.getPosition().getY() >= 3710;
	}
	
	private static Polygon createArea(RSTile northWest, RSTile southEast){
		Polygon polygonArea = new Polygon();
		
		int xBound = Math.abs(southEast.getX() - northWest.getX()),
			yBound = Math.abs(southEast.getY() - northWest.getY());
		
		for(int x = 0; x <= xBound; x++){
			for(int y = 0; y <= yBound; y++){
				int newX = northWest.getX() + x;
				
				polygonArea.addPoint(newX, (int)(northWest.getY() - y));
			}
		}
		
		return polygonArea;
	}
	
	public static boolean isInArea(Polygon area){
		RSTile myPos = Player.getPosition();
		int myX = myPos.getX(),
			myY = myPos.getY();
		
		int[] x = area.xpoints,
			  y = area.ypoints;
		
		for(int z = 0; z < x.length; z ++) {
			if(x[z] == myX && y[z] == myY){
				return true;
			}
		}
		return false;
	}
	
	private void setFoodType() {
		String temp = JOptionPane.showInputDialog("Enter food ID");
		while(temp == null) {
			temp = JOptionPane.showInputDialog("Re-enter food ID");
		}
		food = Integer.parseInt(temp);
	}

	@Override
	public void onPaint(Graphics arg0) {
		// TODO Auto-generated method stub
		double totalXPEarned = (numLittleBones * 4.5) + (numBigBones * 15);
		System.out.println("XP PER HOUR: " + totalXPEarned * (3600 / stopWatch.elapsedTime()));
	}
}