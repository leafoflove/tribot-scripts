package scripts.BoneyardBuryer;
import org.tribot.api.General;
import org.tribot.api.types.generic.Filter;
import org.tribot.api2007.Game;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Options;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSNPC;

public class ObstacleChecker extends Thread {
	
	//also make sure to check until dragon is specific tiles away from you
	//so you don't leave the safespot
	public static boolean isRunning = true;
	@Override
	public void run() {
		while(isRunning) {
			try {
			RSNPC[] npc = NPCs.getAll(new Filter<RSNPC>() {
				@Override
				public boolean accept(RSNPC arg0) {
					// TODO Auto-generated method stub ///originally <= 10
 					//2838 is for grizzly bears
					if(arg0.getPosition().distanceTo(Player.getPosition()) <= 5) {
						return true;
					}
					return false;
				}
			});
			if(npc.length > 0) {
				if(!Game.isRunOn()) {
					Options.setRunOn(true);
				}
			} else {
				Options.setRunOn(false);
			}
			General.sleep(10);
			}catch(Exception e) {}
		}
	}
	
	public void setIsRunning(boolean temp) {
		isRunning = temp;
	}

}
