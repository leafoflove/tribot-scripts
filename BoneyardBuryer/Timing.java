package scripts.BoneyardBuryer;

import static org.tribot.api.General.sleep;

public class Timing {


    public interface Condition {

        public boolean validate();

    }

    public static void CSleep(final Condition c, final long timeout) {

        long startT = System.currentTimeMillis();
        long timeoutT = (System.currentTimeMillis() + timeout) - startT;

        while ((System.currentTimeMillis() - startT) < timeoutT) {
            sleep(50);
            if (c.validate()) break;
        }

    }
}