package scripts.lumbyFisher.api;

import org.tribot.api.interfaces.Positionable;
import org.tribot.api2007.PathFinding;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSObject;

public class MWalking {

	public MWalking() {

		// Needed for unstucking underneath the tower. (caves are not mapped with WebWalking)
		WebWalking.setUseAStar(true);
		WebWalking.setUseRun(false);
	}

	
	public static boolean walkTo(final Positionable posToWalk) {

//		AntibanMgr.doActivateRun();
		System.out.println("Webwalking is running: " + WebWalking.getUseRun());
		return WebWalking.walkTo(posToWalk);

	}

	
	public static boolean canReach(final Positionable toReach) {

		return PathFinding.canReach(toReach, toReach instanceof RSObject);

	}
}