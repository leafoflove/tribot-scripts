package scripts.lumbyFisher;

import java.util.ArrayList;

import org.tribot.api.General;
import org.tribot.api.util.ABCUtil;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Camera;
import org.tribot.api2007.Game;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Options;
import org.tribot.api2007.Player;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSTile;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.MessageListening07;

import scripts.lumbyFisher.api.Banks;
import scripts.lumbyFisher.api.FishingIDs;
import scripts.lumbyFisher.api.MBanking;
import scripts.lumbyFisher.api.MWalking;
import scripts.lumbyFisher.api.Node;

@ScriptManifest (authors = {"musique"}, category = "Skilling", name = "Lumbridge Fisher")
public class LumbyFisher extends Script implements MessageListening07{
	private ABCUtil abc = new ABCUtil();
	public static ArrayList<Node> nodes = new ArrayList<>();
	public static String canReach = "can reach"; //empty means i can reach!
	public static boolean caughtFish = false;
	
	@Override
	public void run() {
		getItems();
    	if(Inventory.isFull()) { //drop shrimps
    		bankItems();
	    }
    	setRunOn();
		MWalking.walkTo(new RSTile(General.random(3237, 3239), General.random(3142, 3146), 0));
		int cameraDegrees = 0;
		while(true) {
		    RSNPC[] fishspot = NPCs.findNearest("Fishing spot");
		    fishspot = filterIsOnScreen(fishspot);
		    System.out.println("FISHING SPOTS: " + fishspot.length);
		    while(fishspot.length == 0) {
		    	Camera.setCameraRotation(cameraDegrees);
		    	fishspot = NPCs.findNearest("Fishing spot");
		    	cameraDegrees++;
		    	if(cameraDegrees == 361) {
		    		//walk back to starting position and restart camera rotation
		    		MWalking.walkTo(new RSTile(General.random(3237, 3239), General.random(3142, 3146), 0));
		    		cameraDegrees = 0;
		    	}
		    }
		    if(fishspot.length != 0) {
		    	RSNPC fishingLocation = fishspot[0];
		    	if (abc.BOOL_TRACKER.USE_CLOSEST.next()) {
		            if (fishspot[1].getPosition().distanceToDouble(fishspot[0]) < 3.0)
		                fishingLocation = fishspot[1];
		        }

		    	if(canReach.equals("can reach")) {
		    	} else { //might run into possible issues here
		    		canReach = "can reach"; //reset
		    		//rewalk back to that location and start fishing again
		    		MWalking.walkTo(new RSTile(General.random(3237, 3239), General.random(3142, 3146), 0));
		    		continue;
		    	}
		    	if(fishingLocation.click("Net")) {
		    		while(!Inventory.isFull() && Player.getAnimation() != -1) {
		    			if(!caughtFish) {
		    				fishingLocation.click("Net");
		    			}
		    			abc.performRotateCamera();
		    			sleep(General.random(5000,6000));	//random sleeping
		    		}
		    	}
		    	//reset caughtFish
		    	caughtFish = false;
//		        }
		    	if(Inventory.isFull()) { //drop shrimps
		    		bankItems();
		    		MWalking.walkTo(new RSTile(General.random(3237, 3239), General.random(3142, 3146), 0));
			    }
		        double d = Math.random();
		        if (d < 0.8) {
		            // 80% chance of being here
		        } else {
		            // 20% chance of being here
		        	abc.performXPCheck(SKILLS.FISHING);
		        }
		    }
	           General.sleep(this.abc.DELAY_TRACKER.SWITCH_OBJECT.next());
	           this.abc.DELAY_TRACKER.SWITCH_OBJECT.reset();
		}
	}
	
    private void setRunOn(){
        if(Game.getRunEnergy() >= abc.INT_TRACKER.NEXT_RUN_AT.next()){
                Options.setRunOn(true);
        }
        abc.INT_TRACKER.NEXT_RUN_AT.reset();
    }
	
	private void getItems() {
		if(!hasItem(FishingIDs.FISH_NET)) {
			MWalking.walkTo(Banks.LUMBRIDGE.getLocation());
			while(!MBanking.openBooth(Banks.LUMBRIDGE)) {
				sleep(General.random(10,20));	//random sleeping
			}
			while(!Banking.isBankScreenOpen()) {
				System.out.println("bank isn'ts open");
				sleep(General.random(10,20));	//random sleeping
			}
			if(Banking.isBankScreenOpen()) {
				System.out.println("BANK IS OPEN");
			}
			sleep(General.random(100,200));	//random sleeping
			RSItem[] item = Banking.getAll();
			System.out.println("item length: " + item.length);
			System.out.println(MBanking.withdrawItem(1, 303));
		}
	}
	
	private void bankItems() {
		MWalking.walkTo(Banks.LUMBRIDGE.getLocation());
		while(!MBanking.openBooth(Banks.LUMBRIDGE)) {
			sleep(General.random(10,20));	//random sleeping
		}
		while(!Banking.isBankScreenOpen()) {
			System.out.println("bank isn'ts open");
			sleep(General.random(10,20));	//random sleeping
		}
		if(Banking.isBankScreenOpen()) {
			System.out.println("BANK IS OPEN");
		}
		sleep(General.random(100,200));	//random sleeping
		RSItem[] item = Banking.getAll();
		System.out.println("item length: " + item.length);
		Banking.depositAllExcept(303); //deposit all except net
	}
	
	private boolean hasItem(int idofitem) { 
		RSItem[] item = Inventory.find(idofitem);
		return item.length > 0;
	}

	@Override
	public void clanMessageReceived(String arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void duelRequestReceived(String arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void personalMessageReceived(String arg0, String arg1) {
		// TODO Auto-generated method stub
		System.out.println("ARG0: " + arg0 + " arg1: " + arg1);
	}

	@Override
	public void playerMessageReceived(String arg0, String arg1) {
		// TODO Auto-generated method stub
		System.out.println("ARG0: " + arg0 + " arg1: " + arg1);
	}

	@Override
	public void serverMessageReceived(String arg0) {
		// TODO Auto-generated method stub
		System.out.println("ARG0: " + arg0);
		if(arg0.equals("I can't reach that!")) {
			canReach = "I can't reach that!";
		}
		if(arg0.contains("catch")) {
			caughtFish = true;
		}
	}

	@Override
	public void tradeRequestReceived(String arg0) {
		// TODO Auto-generated method stub
		System.out.println("ARG0: " + arg0);
	}
	
	public RSNPC[] filterIsOnScreen(RSNPC[] array) {
		ArrayList<RSNPC> temp = new ArrayList<RSNPC>();
		for(RSNPC v : array) {
			if(v.isOnScreen()) {
				temp.add(v);
			}
		}
		RSNPC[] tempArray = new RSNPC[temp.size()];
		for(int i = 0; i < temp.size(); i++) {
			tempArray[i] = temp.get(i);
		}
		return tempArray;
	}
}