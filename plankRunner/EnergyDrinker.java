package scripts.plankRunner;

import org.tribot.api.General;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Game;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.types.RSItem;

public class EnergyDrinker extends Thread {
	
	private int energyDrink4 = 3016;
	private int energyDrink3 = 3018;
	private int energyDrink2 = 3020;
	private int energyDrink1 = 3022;
	private int energyDrink0 = 229;
	public static boolean isDrinking = false;
	
	@Override
	public void run() {
		while(true) {
			if(Game.getRunEnergy() < General.random(50, 60)) {
				isDrinking = true;
				int energyDrinkID = findEnergyDrinkIDInInventory();
//				System.out.println("energy Drink ID" + energyDrinkID);
				if(energyDrinkID == -1) {
					isDrinking = false;
					//end thread no point anymore
					break;
				}
				RSItem drink = Inventory.find(energyDrinkID)[0];
				if(energyDrinkID == energyDrink4) {
					while(!drink.click("Drink Super energy(4)")){};
				} else if(energyDrinkID == energyDrink3) {
					while(!drink.click("Drink Super energy(3)")){};
				} else if(energyDrinkID == energyDrink2) {
					while(!drink.click("Drink Super energy(2)")){};
				} else {
					while(!drink.click("Drink Super energy(1)")){};
				}
				isDrinking = false;
			}
			General.sleep(1000);
		}
	}
	
	//returns -1 if it finds an empty energy bottle
	//checks if inventory has any of the drinks above.
	public int findEnergyDrinkIDInInventory() {
		RSItem[] temp = Inventory.find(energyDrink4);
		if(temp.length > 0) {
			return energyDrink4;
		}
		temp = Inventory.find(energyDrink3);
		if(temp.length > 0) {
			return energyDrink3;
		}
		temp = Inventory.find(energyDrink2);
		if(temp.length > 0) {
			return energyDrink2;
		}
		temp = Inventory.find(energyDrink1);
		if(temp.length > 0) {
			return energyDrink1;
		}
		return -1;
	}
	

}
