package scripts.plankRunner;

import javax.swing.JOptionPane;

import org.tribot.api.General;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Camera;
import org.tribot.api2007.Game;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Options;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSTile;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;

import scripts.plankRunner.Timing.Condition;

@ScriptManifest (authors = {"musique"}, category = "PlankRunner", name = "Plank Runner")

public class PlankRunner extends Script {
	
	private String treeType = null;
	private boolean useEnergyPots = false;
	private int coinsID = 995;
	private int oakID = 1521;
	private int teakID = 6333;
	private int energyDrink4 = 3016;
	private int energyDrink3 = 3018;
	private int energyDrink2 = 3020;
	private int energyDrink1 = 3022;
	private DrinkingCondition drinkingCondition = new DrinkingCondition();

	@Override
	public void run() {
		setTreeType();
		setEnergyPots();
		while(true) {
			if(Game.getRunEnergy() > General.random(15, 25)) {
				setRunOn();
			}
			walkAndOpenBank();
			depositAll();
			//if not enough items, just return
			if(!withdrawEnergyDrinkAndLogsAndCoins()) {
				System.out.println("Not enough resources, Have a good day! :)");
				depositAll();
				return; 
			}
			if(Game.getRunEnergy() > General.random(15, 25)) {
				setRunOn();
			}
			walkToSawmill();
			plank();
		}
	}
	
	public void depositAll() {
		Banking.depositAll();
	}
	
	public void startEnergyDrinkerThread() {
		Thread energyDrinker = new EnergyDrinker();
		energyDrinker.setDaemon(true);
		energyDrinker.start();
	}
	
	public void setRunOn() {
		if(!Game.isRunOn()) {
			while(!Options.setRunOn(true)){};
		}
	}
	
	int inventoryCount = Inventory.getAll().length;
	//true if has enough items (enough coins and logs)
	//false if otherwise
	private boolean withdrawEnergyDrinkAndLogsAndCoins() {
		boolean startEnergyRun = false;
		int numLogsToWithdraw = 26;
		//withdraw energyDrinkIfHave
		if(useEnergyPots) {
			int energyDrinkIDInBank = findEnergyDrinkIDInBank();
			//didn't find it in inventory
			if(energyDrinkIDInBank == -1) {
				numLogsToWithdraw = 27; //set to 27 cuz no energy drink
			} else { //there is energy drink in bank
				while(!MBanking.withdrawItem(1, energyDrinkIDInBank)){};
				startEnergyRun = true;
			}
		} else {
			numLogsToWithdraw = 27; //if not using Energy pots, reset to 27
		}
		if(treeType.equals("oak")) {
			return withDraw(oakID, numLogsToWithdraw, startEnergyRun);
		} else {
			return withDraw(teakID, numLogsToWithdraw, startEnergyRun);
		}
	}
	
	private boolean withDraw(int id, int numLogsToWithdraw, boolean startEnergyRun) {
		RSItem[] temp = Banking.find(id);
		if(temp.length == 0) {
			return false;
		}
		inventoryCount = Inventory.getAll().length;
		while(!MBanking.withdrawItem(numLogsToWithdraw, id)){};
		//not enough logs
		Timing.CSleep(new Condition() {
			@Override
			public boolean validate() {
				return Inventory.getAll().length != inventoryCount;
			}
		}, General.random(1000000, 3600000));
		General.sleep(General.random(1000,2000));			
		temp = Banking.find(coinsID);
		if(temp.length == 0) {
			return false;
		}
		inventoryCount = Inventory.getAll().length;
		withdrawCoins(Inventory.getCount(id) * ((id == oakID) ? 250 : 500));
		Timing.CSleep(new Condition() {
			@Override
			public boolean validate() {
				return Inventory.getAll().length != inventoryCount;
			}
		}, General.random(1000000, 3600000));
		General.sleep(General.random(1000,2000));			
		//not enough coins
		if(Inventory.getCount(coinsID) < ((id == oakID) ? 250 : 500)) {
			return false;
		}
		if(startEnergyRun) {
			startEnergyDrinkerThread();
		}
		while(!Banking.close()){};
		return true;
	}
	
	//returns id of drink
	private int findEnergyDrinkIDInBank() {
		RSItem[] temp = Banking.find(energyDrink4);
		if(temp.length > 0) {
			return energyDrink4;
		}
		temp = Banking.find(energyDrink3);
		if(temp.length > 0) {
			return energyDrink3;
		}
		temp = Banking.find(energyDrink2);
		if(temp.length > 0) {
			return energyDrink2;
		}
		temp = Banking.find(energyDrink1);
		if(temp.length > 0) {
			return energyDrink1;
		}
		return -1;
	}
	
	private void withdrawCoins(int amount) {
		while(!MBanking.withdrawItem(amount, coinsID)){};
	}
	
	private void setTreeType() {
		String temp = JOptionPane.showInputDialog("Enter tree type (oak, teak)");
		while(temp == null || !(temp.equals("Oak") || temp.equals("oak") || temp.equals("Teak") || temp.equals("teak"))) {
			temp = JOptionPane.showInputDialog("Re-enter tree type (oak, teak)");
		}
		treeType = temp.toLowerCase();
	}
	
	private void setEnergyPots() {
		String temp = JOptionPane.showInputDialog("Use Super Energy Pots? (y, n)");
		while(temp == null || !(temp.equals("Y") || temp.equals("y") || temp.equals("N") || temp.equals("n"))) {
			temp = JOptionPane.showInputDialog("Use Super Energy Pots? (y, n)");
		}
		if(temp.toLowerCase().equals("y")) {
			useEnergyPots = true;
		} else {
			useEnergyPots = false;
		}
	}
	
	private void walkToSawmill() {
		while(!WebWalking.walkTo(new RSTile(3302, 3491), drinkingCondition, 10)){};
	}
	
	//convert logs to planks
	private void plank() {
		RSNPC[] sawmillOperator = NPCs.findNearest(5422);
		if(sawmillOperator.length > 0) {
			Camera.turnToTile(sawmillOperator[0].getPosition());
		}
		while(!sawmillOperator[0].click("Buy-plank Sawmill Operator")){};
		Timing.CSleep(new Condition() {

			@Override
			public boolean validate() {
				return Interfaces.get(403, 90) != null;
			}
			
		}, General.random(1000000, 3600000));
		if(treeType.equals("oak")) {
			while(!Interfaces.get(403, 90).click("Buy All")){};
		} else { //it is teak
			while(!Interfaces.get(403, 91).click("Buy All")){};
		}
	}
	
	private void walkAndOpenBank() {
		// TODO Auto-generated method stub
		while(!WebWalking.walkTo(Banks.VARROCK_EAST.getLocation(), drinkingCondition, 10)){};
		while(!MBanking.openBooth(Banks.VARROCK_EAST)) {
			sleep(General.random(500,700));	//random sleeping
		}
		while(!Banking.isBankScreenOpen()) {
			System.out.println("bank isn't open");
			sleep(General.random(10,20));	//random sleeping
		}
		Timing.CSleep(new Condition() {
			@Override
			public boolean validate() {
				return Interfaces.get(12) != null;
			}
		}, General.random(1000000, 3600000));
		General.sleep(General.random(100,200));	//random sleeping
	}

}
