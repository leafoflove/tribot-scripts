package scripts.plankRunner;

import org.tribot.api.interfaces.Positionable;
import org.tribot.api2007.PathFinding;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSObject;

public class MWalking {

	public MWalking() {
		WebWalking.setUseAStar(true);
		WebWalking.setUseRun(false);
	}
	
	public static boolean walkTo(final Positionable posToWalk) {

		System.out.println("Webwalking is running: " + WebWalking.getUseRun());
		return WebWalking.walkTo(posToWalk);

	}

	public static boolean canReach(final Positionable toReach) {

		return PathFinding.canReach(toReach, toReach instanceof RSObject);

	}
}